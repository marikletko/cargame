//
//  PlayViewController.swift
//  CarGame
//
//  Created by Kirill Letko on 11/17/19.
//  Copyright © 2019 Letko. All rights reserved.
//

import UIKit
import CoreMotion

class PlayViewController: UIViewController {
    
    @IBOutlet weak var carView: UIButton!
    
    @IBOutlet weak var firstR: UIImageView!
    
    @IBOutlet var grassView: [UIImageView]!
    
    @IBOutlet weak var barrelView: UIImageView!
    
    @IBOutlet weak var roadView: UIImageView!
    
    let widthScreen = UIScreen.main.bounds.size.width
    let heightScreen = UIScreen.main.bounds.size.height
    private let carChoosed = UserDefaults.standard.string(forKey: "carChoice")
    private let motionManager = CMMotionManager()
    private var timerLine = Timer()
    private var accelTimer = Timer()
    private var timerBarrel = Timer()
    private var timerCheckCrash = Timer()
    private var timerGrassOne = Timer()
    private var timerGrassTwo = Timer()
    private var lineArray:[UIView] = []
    private var score = 0
    private var carHealth = true
    private var currentBarrelLocation:CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    private var currentGrassLocation:[CGRect] = [CGRect(x: 0,y: 0, width: 0, height: 0), CGRect(x: 0,y: 0, width: 0, height: 0)]
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.grassView[0].frame.origin.y = -self.grassView[0].frame.size.height
        self.grassView[1].frame.origin.y = -self.grassView[1].frame.size.height
        self.firstR.frame.origin.y = -self.firstR.frame.size.height
        self.barrelView.frame.origin.y = -self.barrelView.frame.size.height
        currentBarrelLocation = CGRect(x: 0, y: 0, width: barrelView.frame.size.width, height: barrelView.frame.size.height)
        currentGrassLocation[0] = CGRect(x: 0, y: 0, width: grassView[0].frame.size.width, height: grassView[0].frame.size.height)
        currentGrassLocation[1] = CGRect(x: 0, y: 0, width: grassView[1].frame.size.width, height: grassView[1].frame.size.height)
        
        timers()
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        motionManager.startAccelerometerUpdates()
        accelTimer = Timer.scheduledTimer(timeInterval: accelTimerInterval, target: self, selector: #selector(update), userInfo: nil, repeats: true)
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        carView.dropShadow(color: UIColor.black, opacity: carViewOR.0, radius: carViewOR.1)
        barrelView.dropShadow(color: UIColor.black, opacity:barrelViewOR.0, radius: barrelViewOR.1)
        grassView.forEach({$0.dropShadow(color: UIColor.black, opacity: grassViewOR.0, radius: grassViewOR.1)})
        
    }
    
}

private extension PlayViewController {
    
    
    @IBAction func carViewButton(_ sender: Any) {
        carView.isSelected = true
       
        UIView.animate(withDuration: carJumpInterval, animations: {
             self.carView.frame.size.height += jumpCarSize
            self.carView.frame.size.width += jumpCarSize
            self.carView.frame.origin.x -= 10
            self.carView.frame.origin.y -= 10
                              self.carView.isEnabled = false
                              self.carView.isSelected = false
            self.carHealth = false
                              self.motionManager.stopAccelerometerUpdates()
         }, completion: { finished in
            
            UIView.animate(withDuration: carJumpInterval/2) {
                self.carView.frame.size.height -= jumpCarSize
                     self.carView.frame.size.width -= jumpCarSize
                self.carView.frame.origin.x += 10
                self.carView.frame.origin.y += 10
                     self.carView.isEnabled = true
                     self.carView.isSelected = false
                     self.carHealth = true
                     self.motionManager.startAccelerometerUpdates()
            }
     
            
             
         })
        
    }
    
    @objc func update() {
        carView.setImage(UIImage(named:carChoosed!), for: UIControl.State.normal)
        //carView.setImage(UIImage(named:carChoosed), for: UIControl.State.normal)
        currentBarrelLocation = self.barrelView.layer.presentation()!.frame
        currentGrassLocation[0] = self.grassView[0].layer.presentation()!.frame
        currentGrassLocation[1] = self.grassView[1].layer.presentation()!.frame
         carView.imageView?.contentMode = .scaleAspectFill
        carView.imageView?.clipsToBounds = false
        if let accelerometerData = motionManager.accelerometerData?.acceleration.x {
            if(accelerometerData >= 0.15) {
                carView.setImage(UIImage(named:carChoosed!), for: UIControl.State.normal)
                let newImage = carView.image(for:UIControl.State.normal)?.rotate(carRotateRightAngle)
                carView.setImage(newImage, for: UIControl.State.normal)
                goCarRight()
                 
            } else if accelerometerData <= -0.15 {
                carView.setImage(UIImage(named: carChoosed!), for: UIControl.State.normal)
                let newImage = carView.image(for:UIControl.State.normal)?.rotate(carRotateLeftAngle)
                carView.setImage(newImage, for: UIControl.State.normal)
                goCarLeft()
                
            } else {
           //   let car = UserDefaults.standard.value(BaseElement.self, forKey: "carChoice") //
           //     print(car?.carChoice ?? "dsad")
                carView.setImage(UIImage(named:carChoosed!), for: UIControl.State.normal)
                 
            }
        }
    }
    
    
    func timers() {
        timerCheckCrash = Timer.scheduledTimer(withTimeInterval: timerCheckCrashInterval, repeats: true, block: { (_) in
              if self.carHealth == true {
              if(self.carView.frame.intersects(self.currentBarrelLocation)) {
                  self.lostGame()
              }
              
              if(self.carView.frame.intersects(self.currentGrassLocation[0]) || self.carView.frame.intersects(self.currentGrassLocation[1])) {
                  self.lostGame()
              }
              
              if(self.carView.frame.origin.x < 0 || self.carView.frame.origin.x + self.carView.frame.size.width > self.widthScreen) {
                  self.lostGame()
              }
              }
          })
          timerCheckCrash.fire()
          
          
          timerBarrel = Timer.scheduledTimer(withTimeInterval: timerBarrelInterval, repeats: true, block: { (_) in
              self.goBarrel()
              
          })
          timerBarrel.fire()
          
          timerLine = Timer.scheduledTimer(withTimeInterval: timerLineInterval, repeats: true, block: { (_) in
              self.goFirst()
              self.score += 1
          })
          timerLine.fire()
          
          
          timerGrassOne = Timer.scheduledTimer(withTimeInterval: TimeInterval(Float.random(in:2...4)), repeats: true, block: { (_) in
              self.goGrassOne()
          })
          timerGrassOne.fire()
          
          
          timerGrassTwo = Timer.scheduledTimer(withTimeInterval: TimeInterval(Float.random(in:2...4)), repeats: true, block: { (_) in
              self.goGrassTwo()
          })
          timerGrassTwo.fire()
    }
    
    func lostGame() {
        self.timerCheckCrash.invalidate()
        self.timerLine.invalidate()
        self.timerBarrel.invalidate()
        self.accelTimer.invalidate()
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "GameOverViewController") as? GameOverViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
        controller.score = score
    }
    
    func goGrassTwo() {
        
        UIView.animate(withDuration: goGrassDuration, delay: 0, animations: {
            self.grassView[1].frame.origin.y = self.heightScreen
        }, completion: { finished in
            self.grassView[1].frame.origin.y = -self.grassView[1].frame.size.height
        })
        
    }
    
    func goGrassOne() {
        
        UIView.animate(withDuration: goGrassDuration, delay: 0, animations: {
            self.grassView[0].frame.origin.y = self.heightScreen
        }, completion: { finished in
            self.grassView.first?.frame.origin.y = -self.grassView[0].frame.size.height
        })
        
    }
    
    func goBarrel() {
        self.barrelView.frame.origin.x = CGFloat.random(in: grassView[1].frame.size.width..<widthScreen - barrelView.frame.size.width - grassView[0].frame.size.width)
        
        UIView.animate(withDuration: goBarrelDuration, delay: 0, animations: {
            
            self.barrelView.frame.origin.y = self.heightScreen
        }, completion: { finished in
            self.barrelView.frame.origin.y = -self.barrelView.frame.size.height
        })
        
    }
    
    func goFirst() {
        
        let line = UIView()
        line.frame = CGRect(x: firstR.frame.origin.x, y: -firstR.frame.size.height, width: firstR.frame.size.width, height: firstR.frame.size.height)
        line.backgroundColor = .white
        self.roadView.addSubview(line)
        self.lineArray.append(line)
        UIView.animate(withDuration: goFirstDuration) {
            self.lineArray.last?.frame.origin.y = self.heightScreen
        }
    }
    
    
    func goCarRight() {
        
        UIView.animate(withDuration: goCarSideDuration, delay: 0, animations: {
            self.carView.frame.origin.x += carViewMoveX
            if self.carView.frame.intersects(self.grassView[1].frame) {
                self.lostGame()
            }
        }, completion: { finished in
            
        })
    }
    
    func goCarLeft() {
        
        UIView.animate(withDuration: goCarSideDuration, delay: 0, animations: {
            self.carView.frame.origin.x -= carViewMoveX
            if self.carView.frame.intersects(self.grassView[0].frame) {
                self.lostGame()
            }
        }, completion: { finished in
            
        })
    }
    
    
    
    
    

 }

        



