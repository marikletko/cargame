//
//  UserDefaults.swift
//  CarGame
//
//  Created by Kirill Letko on 12/15/19.
//  Copyright © 2019 Letko. All rights reserved.
//

import Foundation
import UIKit

class BaseElement: Codable {
    
    var userName: String?
    var userScore: Int?
  //    var carChoice: String?
    
    init() {}
    
    private enum CodingKeys: String, CodingKey {
        case userName
        case userScore
   //    case carChoice
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        userName = try container.decodeIfPresent(String.self, forKey: .userName)
        userScore = try container.decodeIfPresent(Int.self, forKey: .userScore)
       //  carChoice = try container.decode(String.self, forKey: .carChoice)
    }
}


