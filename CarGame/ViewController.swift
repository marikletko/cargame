//
//  ViewController.swift
//  CarGame
//
//  Created by Kirill Letko on 11/16/19.
//  Copyright © 2019 Letko. All rights reserved.
//

import UIKit
import Rswift
@available(iOS 12.4, *)
class ViewController: UIViewController {
                  
    @IBOutlet weak var recordLabel: UILabel!
    @IBOutlet weak var soundTurnOutlet: UIButton!
    var soundOn = true
    
    
     

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
           backgroundImage.image = UIImage(named: "background.png")
           backgroundImage.contentMode = UIView.ContentMode.scaleToFill
           self.view.insertSubview(backgroundImage, at: 0)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        let record = UserDefaults.standard.integer(forKey: "record")
        recordLabel.text = "\(record)"
    }
    
    
    @IBAction func recordButton(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "RecordsViewController") as? RecordsViewController else { return }
               self.navigationController?.pushViewController(controller, animated: true)
       }
   
    
    
    @IBAction func soundTurn(_ sender: Any) {
        
        if soundOn == true {
            (sender as AnyObject).setBackgroundImage(UIImage(named: "soundOff"), for: UIControl.State.normal)
            soundOn = false
        }
        else {
              (sender as AnyObject).setBackgroundImage(UIImage(named: "sound"), for: UIControl.State.normal)
            soundOn = true
        }
        
    }
    
    @IBAction func playButton(_ sender: Any) {

        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "PlayViewController") as? PlayViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func settingsButton(_ sender: Any) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
}


