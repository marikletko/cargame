//
//  Constants.swift
//  CarGame
//
//  Created by Kirill Letko on 12/6/19.
//  Copyright © 2019 Letko. All rights reserved.
//

import Foundation
import UIKit

let jumpCarSize:CGFloat = 20
let carViewOR:(Float,CGFloat) = (0.2,5)
let barrelViewOR:(Float,CGFloat) = (0.2,5)
let grassViewOR:(Float,CGFloat) = (0.2,5)
let accelTimerInterval = 0.1
let carJumpInterval:TimeInterval = 0.8
let carRotateRightAngle:CGFloat = -.pi/6
let carRotateLeftAngle:CGFloat = .pi/6
let timerCheckCrashInterval:TimeInterval = 0.1
let timerBarrelInterval:TimeInterval = 5
let timerLineInterval:TimeInterval = 1
let goGrassDuration:TimeInterval = 2
let goBarrelDuration:TimeInterval = 2
let goFirstDuration:TimeInterval = 3
let goCarSideDuration:TimeInterval = 0.1
let carViewMoveX:CGFloat = 20
let carsStringArray = ["Car", "CarTwo"]
