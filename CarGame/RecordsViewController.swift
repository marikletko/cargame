//
//  RecordsViewController.swift
//  CarGame
//
//  Created by Kirill Letko on 12/15/19.
//  Copyright © 2019 Letko. All rights reserved.
//

import UIKit

class RecordsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let arrayOfScores:[BaseElement] = UserDefaults.standard.value([BaseElement].self, forKey: "scores") ?? []
    
    
    @IBOutlet weak var recordsTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfScores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellFire", for: indexPath)
        let scoresLine = "Name: \(arrayOfScores[indexPath.row].userName ?? "")" + "                   " + "Score: \(arrayOfScores[indexPath.row].userScore ?? 0)"
        
        cell.textLabel?.text = scoresLine
        return cell
    }


    
    @IBAction func homeButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
     }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
