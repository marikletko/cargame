//
//  SettingsViewController.swift
//  CarGame
//
//  Created by Kirill Letko on 11/17/19.
//  Copyright © 2019 Letko. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBAction func carFirst(_ sender: Any) {
        UserDefaults.standard.set(carsStringArray.first, forKey: "carChoice")
        
    //    let BaseObj = BaseElement()
    //    BaseObj.carChoice = car
    //    UserDefaults.standard.set(encodable: car, forKey: "carChoice")
    }
    
    @IBAction func carSecond(_ sender: Any) {
          UserDefaults.standard.set(carsStringArray[1], forKey: "carChoice")
     //   let car = "CarTwo"
     //   let BaseObj = BaseElement()
     //   BaseObj.carChoice = car
     //   UserDefaults.standard.set(encodable: car, forKey: "carChoice")
    }
     
    @IBOutlet weak var settingsView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let backgroundImage = UIImageView(frame: settingsView.bounds)
        backgroundImage.image = UIImage(named: "settingsBack")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFit
        settingsView.insertSubview(backgroundImage, at: 0)

        // Do any additional setup after loading the view.
    }

    @IBAction func homeButton(_ sender: Any) { self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
