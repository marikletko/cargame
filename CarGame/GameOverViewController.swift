//
//  GameOverViewController.swift
//  CarGame
//
//  Created by Kirill Letko on 12/1/19.
//  Copyright © 2019 Letko. All rights reserved.
//

import UIKit

class GameOverViewController: UIViewController {
    
    @IBOutlet weak var scoreLabel: UILabel!
    var score = 0
    var BaseObj = BaseElement()
    var maxScore:Int = 0
    var arrayOfScores:[BaseElement] = UserDefaults.standard.value([BaseElement].self, forKey: "scores") ?? []
    @IBAction func playAgainButton(_ sender: Any) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "PlayViewController") as? PlayViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func homeButton(_ sender: Any) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func settingsButton(_ sender: Any) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "background.png")
        backgroundImage.contentMode = UIView.ContentMode.scaleToFill
        self.view.insertSubview(backgroundImage, at: 0)
        scoreLabel.text = "\(score)"
        
        showInputDialog(title: "Add score",
                        subtitle: "Please enter the name to save your score.",
                        actionTitle: "Add",
                        cancelTitle: "Cancel",
                        inputPlaceholder: "New number",
                        inputKeyboardType: .numberPad)
        { (namePlayer:String?) in
            print(" \(namePlayer ?? "")")
            
            self.BaseObj.userName = namePlayer
            self.BaseObj.userScore = self.score
            
            self.arrayOfScores.append(self.BaseObj)
            UserDefaults.standard.set(encodable: self.arrayOfScores, forKey: "scores")
            
            
        }
        
        var scoresArray:[Int] = []
        
        arrayOfScores.forEach({ element in
            scoresArray.append(element.userScore ?? 0)
        })
        
        maxScore = scoresArray.max() ?? 0
        
        UserDefaults.standard.set(maxScore, forKey: "record")
        
    }
}



